using System;
using UnityEngine;

public class MobileInput : IInput
{
    public event Action<Vector3> ClikDown;
    public event Action<Vector3> ClikUp;
    public event Action<Vector3> Drag;
}
