﻿using System;
using UnityEngine;

public interface IInput
{
    public event Action<Vector3> ClikDown;
    public event Action<Vector3> ClikUp;
    public event Action<Vector3> Drag;
}