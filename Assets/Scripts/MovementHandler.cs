﻿using System;
using UnityEngine;
using Zenject;

public class MovementHandler : ITickable, IDisposable, IInitializable
{
    private IInput _input;
    private IMovable _movable;

    public MovementHandler(IInput input, IMovable movable)
    {
        _input = input;
        _movable = movable;

        Debug.Log(_input.GetType());
        Debug.Log($"Скорость полученного IMovable: {_movable.Speed}");

        _input.ClikDown += OnClickDown;
        _input.ClikUp += OnClickUp;
        _input.Drag += OnDrag;
    }

    private void OnClickDown(Vector3 position)
    {
        //Обработка нажатия
    }
    private void OnClickUp(Vector3 position)
    {
        //Обработка нажатия
    }
    private void OnDrag(Vector3 position)
    {
        //Обработка нажатия
    }

    public void Dispose()
    {
        _input.ClikDown -= OnClickDown;
        _input.ClikUp -= OnClickUp;
        _input.Drag -= OnDrag;
    }

    public void Tick()
    {
        
    }

    public void Initialize()
    {
        
    }
}